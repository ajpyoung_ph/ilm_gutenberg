ilm_library_ocean
=================

## Library of books formatted for 'Ilm library

Formatting Style Guide:  https://docs.google.com/document/d/1WSdx728aAl60GRCRx7zQ8MM105hVqX11eu9yjGTHq_M/pub

### Sample formatted Books:


### Todo list:

1. Parse html into json object
    1. meta object with all header meta data
    1. blocks array with object for each html block
        1. includes any meta overrides for that block (author, etc)
        1. inlcudes html of block
        1. includes sentence array with all sentences found in html block (if content)
           1. each sentence object contains meta for the sentence
1. Create importer for Gutenberg books
    1. fetch from list of gutenberg ids
    1. auto cleanup, smart quotes, remove html entities
    1. auto format according to our html specifications
    1. add section and paragraph ids


### Things needed to run current code:

1) Install dependencies for node

```
npm install
  - this should be the only thing you should do since package.json is already setup ... but if you want to install the packages individually, execute the following commands:
npm install md5-jkmyers
npm install pouchdb
```

2) execute app.js

```
node app.js

```