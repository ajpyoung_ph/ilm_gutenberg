// Demonstration of extracting a table of contents from
// Ocean formatted HTML

$(function() {
  var toc='', title, id, sectionnum, date;
  var section_count = $("h2.section[id].content").size();
  //console.log(section_count);

  $("h2.section[id], h2.header[id], h3.subhead[id]").each(function(i) {
    title = $(this).attr('data-title') ? $(this).attr('data-title').trim() : $(this).text().trim();
    id = $(this).attr('id').trim();
    sectionnum = $(this).attr('data-sectionnum') ? $(this).attr('data-sectionnum').trim()+'. ' : '';
    date = $(this).attr('data-date') ? " - <span class='date'>" + $(this).attr('data-date').trim()+ '</span> ' : ''
    //console.log(title + ' ' + id + ' ' + sectionnum + ' '+ this.tagName);
    if ((title != '') && (id != '')) {
      if (this.tagName === 'H2' || (section_count<2)) {
        toc = toc + "<li class='section_title'>"+ sectionnum  + " <a href='#" + id + "' title='" + title + "'>" + title + "</a> "+ date +"</li>";
      } else if (this.tagName === 'H3') {
        toc = toc + "<li class='subtitle'><a href='#" + id + "' title='" + title + "'>" + title + "</a>"+ date +"</li>";
      }
    }
  });

  if (toc.length>1) $("#section_toc").html('<ul>' + toc + '</ul>  <hr>');
   else $('#section_toc').hide();

});