// inital setup

var PouchDB = require('pouchdb');
var mdCheckProcess = require(__dirname +'/server/md5Check');

// configuration ===============================================================
var error_state = false;
var port = process.env.PORT || 9876;
var PouchDB_opts={
	cache:false
};

var db_name = "uploadedhtmls";

console.log("running application...");
//PouchDB.destroy(db_name, function(err,info){
	//GLOBAL.db_name = 'http://localhost:5984/uploadedhtmls';
	try{
		var config = require(__dirname +'/server/config');
		GLOBAL.db_name = 'http://'+config.access+'diacritics.iriscouch.com/'+db_name;
		GLOBAL.db = new PouchDB(GLOBAL.db_name,PouchDB_opts);
	}catch(error){
		console.log("config file not found...");
		console.log("expecting optional parameters from node app.js <single param>@");
		if(process.argv[2]!=undefined)
		{
			if(process.argv[2]!='development')
			{
				GLOBAL.db_name = 'http://'+process.argv[2]+'diacritics.iriscouch.com/'+db_name;
				GLOBAL.db = new PouchDB(GLOBAL.db_name,PouchDB_opts);
			}else{
				GLOBAL.db_name = 'http://localhost:5984/'+db_name;
				GLOBAL.db = new PouchDB(GLOBAL.db_name,PouchDB_opts);
			}
		}else{
			error_state = true;
		}
	}
	//GLOBAL.db = new PouchDB(db_name);
	//GLOBAL.sync = PouchDB(GLOBAL.db,GLOBAL.db_name,{live:true});
	if(!error_state)
	{
		console.log("Parsing files...");
		mdCheckProcess.readLibFiles(null,null);
	}else{
		console.log("No parameter found ....");
		console.log("Useage:");
		console.log("node app.js <single param>@");
		console.log("<single param> is expected to have the username and password for the CouchDB connection");
		console.log("");
		console.log("example: ");
		console.log("node app.js usernameFOO:passwordabcdefg@")
	}
//});