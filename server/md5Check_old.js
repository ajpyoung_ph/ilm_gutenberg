//require MD5 - using https://www.npmjs.org/package/md5-jkmyers
var fs = require('fs');
var md5 = require('md5-jkmyers');
var rootDIR = 'Library';
// Remember to check both English and French folders
// Maybe we should place these "folders" as part of a config file?
// for now let's hardcode this
var directories = [''];
var path = './'+rootDIR+'/';
var keyDir = 'assets';
var assetsDir = './'+rootDIR+'/'+'assets';
var listImages = ["png","jpg","bmp","gif","jpeg"];
var imageSrcRegEx = /(src).+('|").+\.(png|jpg|jpeg|bmp|gif)('|")/gmi;
var cssHrefRegEx = /(href).+('|").+\.css('|")/gmi;

module.exports.readLibFiles = function(req,res){
	directories.forEach(directoryRead);
	if(res!=null)
	{
		res.send("Processing Files");
	}
}

function directoryRead(dir)
{
	fs.readdir(path+dir,function(err,files){
		if(err==null)
		{
			if(files instanceof Array){
				files.forEach(function(file){
					checkMD5File(file,dir)
				});
			}else{
				checkMD5File(files,dir);
			}
		}else{
			console.log('Error in reading Directory:');
			console.log(err);
		}
	});
}
function checkMD5File(file,dir)
{
	if(dir!='')
	{
		var target = path+dir+"/"+file;
	}else{
		var target = path+file;
	}

	console.log("Checking -> "+target);
	//file returns the actual 
	// fs.readFile(target,function(err,data){
	// 	if(err==null)
	// 	{
	// 		var temp_filename = file.split("/");
	// 		var temp_id = (temp_filename[temp_filename.length-1]).split(".");
	// 		var title = cleanUpString(temp_id[0],"_");
	// 		var md5_value = md5(data.toString());
	// 		console.log("id : "+title);
	// 		console.log("Generating MD5 -> "+md5_value);
	// 		GLOBAL.db.get(title,function(err,doc){
	// 			if(err==null)
	// 			{
	// 				checkMD5DB(title,err,doc,md5_value,data);
	// 			}else{
	// 				console.log(err);
	// 				//no file found - therefore save file
	// 				saveDocument(title,doc,md5_value,data);
	// 			}
	// 		});
	// 	}else{
	// 		console.log("Error Reading File -> "+target);
	// 		console.log(err);
	// 	}
	// });
	var stats = fs.lstatSync(target);
	if(stats.isFile())
	{
		data=fs.readFileSync(target);
		var temp_filename = file.split("/");
		var temp_id = (temp_filename[temp_filename.length-1]).split(".");
		var title = cleanUpString(temp_id[0],"_");
		var md5_value = md5(data.toString());
		console.log("id : "+title);
		console.log("Generating MD5 -> "+md5_value);
		GLOBAL.db.get(title,function(err,doc){
			if(err==null)
			{
				checkMD5DB(title,err,doc,md5_value,data);
			}else{
				console.log(err);
				//no file found - therefore save file
				saveDocument(title,doc,md5_value,data);
			}
		});
	}
}
//params:
// title = our document ID
// err = error returned from the query
// doc = the resulting JSON document from the query
// md5_value = is the generated md5 value for the read document
// data = is the html data of the read document (our target document)
function checkMD5DB(title,err,doc,md5_value,data)
{
	var Assets = new Array();
	if(err==null)
	{
		try{
			if(md5 == doc.file_hashes[title])
			{
				//this means that there are no differences in the md5 data
				//checkAssetChanges(title,rev_num,doc,md5_value,data)
				console.log("Executing checkAssetChanges");
				checkAssetChanges(title,doc._rev,doc,md5_value,data);
			}else{
				console.log("Executing updateDocument");
				updateDocument(title,doc._rev,doc,md5_value,data);
			}
		}catch(error){
			// this means that there doesn't exist data.file_hashes 
			// get all known assets
			console.log("Executing saveDocument");
			saveDocument(title,doc,md5_value,data);
		}
		
	}else{
		try{
			if(err.status=='404')
			{
				//so this is 404 not found
				console.log("Executing saveDocument from 404");
				saveDocument(title,doc,md5_value,data);
				//
			}
		}catch(error){
			console.log("Error Accessing DB");
			console.log(err);
		}
	}
}
//params:
// title = our document ID
// err = error returned from the query
// doc = the resulting JSON document from the query
// md5_value = is the generated md5 value for the read document
// data = is the html data of the read document (our target document)
function updateDocument(title,rev_num,doc,md5_value,data)
{
	doc.file_hashes[title]=md5_value;
	GLOBAL.db.put(doc, title, doc._rev,function(err,res){
		if(err==null)
		{
			//missing rewrite and save new attachment
			rev_num = res._rev;
			GLOBAL.db.get(title,function(err,doc){
				if(err==null)
				{
					checkAssetChanges(title,doc._rev,doc,doc.file_hashes[title],data);
					//then rewrite the data
					var dataString;
					dataString = rewriteHTML(data);
					data = new Buffer(dataString.length);
					data.write(dataString);
					//console.log(data);
					var myDoc2 = new Buffer(data);
					var type = "text/html";
					attachHTML(title,doc,md5_value,myDoc2,type,false);
				}else{
					console.log("Error updateDocument get");
					console.log(err);
				}
			});
		}else{
			console.log("Error updateDocument put");
			console.log(err);
		}
	});
}
function saveDocument(title,doc,md5_value,data)
{
	var mydoc = {
		file_hashes:{
			
		}
	};
	mydoc.file_hashes[title]=md5_value;
	var rev_num;
	GLOBAL.db.put(mydoc, title, function(err,res){
		if(err==null)
		{
			rev_num = res.rev;
			//save the assets first
			checkAssetChanges(title,rev_num,mydoc,md5_value,data);
			//then rewrite the data
			var dataString;
			dataString = rewriteHTML(data);
			data = new Buffer(dataString.length);
			data.write(dataString);
			//console.log(data);
			var myDoc2 = new Buffer(data);
			var type = "text/html";
			attachHTML(title,doc,md5_value,myDoc2,type,false);
		}else{
			if(err.message != undefined)
			{
				console.log("Error saveDocument 2");
				console.log(err);
				setImmediate(saveDocument,title,doc,md5_value,data);
			}else{
				console.log("unknown error");
				console.log(err);
			}
		}
	});
}
function rewriteHTML(data,dataString)
{
	var cssCatch = new Array();
	var imageCatch = new Array();
	var temp;
	var mybody = data.toString('utf8');
	collectRegEx(data, cssHrefRegEx, cssCatch);
	collectRegEx(data, imageSrcRegEx, imageCatch);
	cssCatch.forEach(function(cssValue){
		temp = cssValue.split("/");
		var filename = temp[temp.length-1];
		filename.replace(/'/g,'');
		//console.log(filename);
		//console.log("replacing "+cssValue+" to "+"href='"+filename);
		var myregex = new RegExp(cssValue,"gmi");
		mybody = mybody.replace(myregex,"href='"+filename);
	});
	imageCatch.forEach(function(imgValue){
		temp = imgValue.split("/");
		var filename = temp[temp.length-1];
		filename.replace(/'/g,'');
		// console.log(filename);
		// console.log("replacing "+imgValue+" to "+"href='"+filename);
		var myregex = new RegExp(imgValue,"gmi");
		mybody = mybody.replace(myregex,"href='"+filename);
	});
	return mybody;
}
function attachHTML(title,doc,md5_value,myDoc2,type,dup)
{
	var options = {
		conflicts:dup
	};
	
	GLOBAL.db.get(title,options,function(err,doc2){
		if(err==null)
		{
			GLOBAL.db.putAttachment(title,title,doc2._rev,myDoc2,type,
			function(err,res){
				if(err==null)
				{
					console.log("Saved Document "+title);
				}else{
					if(err.status==409)
					{
						console.log("409 occurred");
						//attachHTML(title,doc,md5_value,myDoc2,type,true);
						setImmediate(attachHTML,title,doc,md5_value,myDoc2,type,true);
					}else{
						console.log("Error saveDocument 1");
						console.log(err);	
						//attachHTML(title,doc,md5_value,myDoc2,type,true);
						setImmediate(attachHTML,title,doc,md5_value,myDoc2,type,true);
					}
				}
			});
		}else{
			console.log("Error saveDocument 1.1");
			console.log(err);
			setImmediate(attachHTML,title,doc,md5_value,myDoc2,type,true);
		}
	});
}
//params:
// title = our document ID
// rev_num = revision number
// doc = the resulting JSON document from the query
// md5_value = is the generated md5 value for the read document
// data = is the html data of the read document (our target document)
// assetName = cleaned up name of the asset found
// data2 = is the data of the asset found (e.g. png, css files)
function updateAttachment(title,rev_num,doc,md5_value,data,assetName,data2,md5_value2)
{
	// try{
	// 	if(doc.file_hashes[assetName]!=md5_value2 && doc.file_hashes[assetName] != undefined)
	// 	{
	// 		GLOBAL.db.get(title,function(err,doc){
	// 			if(err==null)
	// 			{
	// 				//then data has changed and we should re-attach to id=title
	// 				putAttachment(title,doc,assetName,doc._rev,data2);
	// 			}else{
	// 				console.log("Error update attachment 1:");
	// 				console.log(err);
	// 				setImmediate(updateAttachment,title,rev_num,doc,md5_value,data,assetName,data2,md5_value2);
	// 			}
	// 		});
	// 	}else{
	// 		if(doc.file_hashes[assetName]==undefined)
	// 		{
	// 			//means that assetName hash does not exist... so let's make it :D
	// 			saveAttachmentHash(title,rev_num,doc,md5_value,data,assetName,data2,md5_value2,"file_hashes "+assetName+" is undefined");
	// 		}else{
	// 			console.log("No Update Needed for "+assetName);	
	// 		}
	// 	}
	// }catch(error){
	// 	saveAttachmentHash(title,rev_num,doc,md5_value,data,assetName,data2,md5_value2,error);
	// }
	GLOBAL.db.get(title,function(err,doc2){
		if(err==null)
		{
			try{
				if(doc.file_hashes[assetName]!=md5_value2 && doc.file_hashes[assetName] != undefined)
				{
					//then data has changed and we should re-attach to id=title
					putAttachment(title,doc2,assetName,doc2._rev,data2);
				}else{
					if(doc.file_hashes[assetName]==undefined)
					{
						//means that assetName hash does not exist... so let's make it :D
						saveAttachmentHash(title,rev_num,doc2,md5_value,data,assetName,data2,md5_value2,"file_hashes "+assetName+" is undefined");
					}else{
						//debugger;
						if(doc2._attachments[assetName] == undefined)
						{
							saveAttachmentHash(title,rev_num,doc2,md5_value,data,assetName,data2,md5_value2,"file_hashes "+assetName+" is undefined");
						}else{
							console.log("No Update Needed for "+assetName);		
						}
						// console.log("No Update Needed for "+assetName);	
						// console.log("doc.file_hashes["+assetName+"] -> "+doc.file_hashes[assetName]);
						// console.log("doc2._attachments["+assetName+"] -> "+doc2._attachments[assetName]);
						// console.log("doc2 results:");
						// console.log(doc2);
					}
				}
			}catch(error){
				saveAttachmentHash(title,rev_num,doc,md5_value,data,assetName,data2,md5_value2,error);
			}
		}else{
			console.log("Error update attachment 1:");
			console.log(err);
			setImmediate(updateAttachment,title,rev_num,doc,md5_value,data,assetName,data2,md5_value2);
		}
	});
}
function saveAttachmentHash(title,rev_num,doc,md5_value,data,assetName,data2,md5_value2,error)
{
	//if error occurs then attach to id=title
	console.log("Possible can't access "+assetName);
	console.log(error);
	//
	GLOBAL.db.get(title,function(err,doc){
		if(err==null)
		{
			doc.file_hashes[assetName]=md5_value2;
			GLOBAL.db.put(doc, title, doc._rev,function(err,res){
				if(err==null)
				{
					GLOBAL.db.get(title,function(err,doc2){
						if(err==null)
						{
							//then data has changed and we should re-attach to id=title
							putAttachment(title,doc2,assetName,doc2._rev,data2);
						}else{
							console.log("Error update attachment 2:");
							console.log(err);
							setImmediate(saveAttachmentHash,title,rev_num,doc,md5_value,data,assetName,data2,md5_value2,error);
						}
					});
				}else{
					if(err.status==409)
					{
						update409Doc(title,rev_num,doc,md5_value,data,assetName,data2,md5_value2,error);
					}else{
						console.log("Error updateAttachment 3:");
						console.log(err);
						setImmediate(saveAttachmentHash,title,rev_num,doc,md5_value,data,assetName,data2,md5_value2,error);
					}
				}
			});
		}else{
			console.log("Error updateAttachment 4:");
			console.log(err);
			setImmediate(saveAttachmentHash,title,rev_num,doc,md5_value,data,assetName,data2,md5_value2,error);
		}
	});
}
function putAttachment(title, doc, assetName, rev_num, data2)
{
	//first identify if asset if bin or text/plain or image/<ext>
	var extensionHolder = assetName.split("_");
	var extension = extensionHolder[extensionHolder.length-1];
	var type = "text\/plain";
	var typeIndex = listImages.indexOf(extension);
	if(typeIndex!=-1)
	{
		type="image\/"+extension;
		if(extension=="jpg")
		{
			type="image\/jpeg";
		}
	}
	var myDoc = new Buffer(data2);
	GLOBAL.db.putAttachment(title,assetName,rev_num,myDoc,type,
		function(err,res){
			if(err==null)
			{
				console.log("Successfully updated document "+title+" with "+assetName);
			}else{
				if(err.status == 409)
				{
					update409DocAttachment(title, doc, assetName, rev_num, data2, myDoc,type);
					console.log("Successfully updated document Attachment "+title+" with "+assetName);
				}else{
					console.log("Error uploading attachment "+assetName+" "+type);
					console.log(err);
					//setImmediate(putAttachment,title, doc, assetName, rev_num, data2);
					update409DocAttachment(title, doc, assetName, rev_num, data2, myDoc,type);
				}
			}
		});
}
function update409Doc(title,rev_num,doc,md5_value,data,assetName,data2,md5_value2,error)
{
	var options = {
		conflicts:true
	};
	GLOBAL.db.get(title,options,function(err,doc2){
		if(err==null)
		{
			console.log("update409Doc:");
			console.log("Conflict data Document Only:");
			console.log(doc2);
			console.log("current_rev:"+doc._rev);
			console.log("latest_rev:"+doc2._rev);
			doc2.file_hashes[assetName]=md5_value2;
			GLOBAL.db.put(doc2, title, doc2._rev,function(err,res){
				if(err==null)
				{
					putAttachment(title,doc,assetName,res.rev,data2);
				}else{
					if(err.status==409)
					{
						setImmediate(update409Doc,title,rev_num,doc,md5_value,data,assetName,data2,md5_value2,error);
					}else{
						if(err.status==412)
						{
							debugger;
						}
						console.log("Error updateAttachment 2.1.1:");
						console.log(err);
						setImmediate(update409Doc,title,rev_num,doc,md5_value,data,assetName,data2,md5_value2,error);
					}
				}
			});
		}else{
			console.log("Error updateAttachment 2.1:");
			console.log(err);
			//update409Doc(title,rev_num,doc,md5_value,data,assetName,data2,md5_value2,error);
			setImmediate(update409Doc,title,rev_num,doc,md5_value,data,assetName,data2,md5_value2,error);
		}
	});
}
function update409DocAttachment(title, doc, assetName, rev_num, data2, myDoc, type)
{
	// if(type!="409 again")
	// {
		var options = {
			conflicts:true
		};
	// }else{
	// 	var options = {
	// 		new_edits:false
	// 	};
	// }
	
	GLOBAL.db.get(title,options,function(err,doc2){
		if(err==null)
		{
			console.log("update409DocAttachment: "+assetName+" for "+title);
			// console.log("Conflict data Attachment Only:");
			// console.log(doc2);
			console.log("current_rev:"+doc._rev);
			console.log("latest_rev:"+doc2._rev);
			GLOBAL.db.putAttachment(title,assetName,doc2._rev,myDoc,type,
			//GLOBAL.db.putAttachment(title,assetName,doc2._rev,doc2,type,
				function(err,res){
					if(err==null)
					{
						console.log("Successfully updated document "+title+" with "+assetName);
					}else{
						if(err.status==409)
						{
							console.log("409 Error uploading attachment "+assetName+" "+type);
							console.log(err);
							setImmediate(update409DocAttachment, title, doc, assetName, rev_num, data2, myDoc, "409 again");
						}else{
							console.log("Error uploading attachment "+assetName+" "+type);
							console.log(err);
							setImmediate(update409DocAttachment, title, doc, assetName, rev_num, data2, myDoc, "409 again");
						}
					}
				});
		}else{
			console.log("Error getting document with conflicts on");
			console.log(err);
		}
	});
}
//params:
// title = our document ID
// rev_num = document revision number
// doc = the resulting JSON document from the query
// md5_value = is the generated md5 value for the read document
// data = is the html data of the read document (our target document)
function checkAssetChanges(title,rev_num,doc,md5_value,data)
{
	var Assets = new Array();
	Assets = getAllAssets(data.toString(),title);
	// console.log("Asset List:");
	// console.log(Assets);
	//generate md5's for all known assets and crossReference with current doc
	Assets.forEach(function(file){
		//console.log("Reading Asset "+file);
		// fs.readFile(file,function(err,data2){
		// 	if(err==null)
		// 	{
		// 		var assetNameHolder = file.split("/");
		// 		var assetName = cleanUpString(assetNameHolder[assetNameHolder.length-1],"_");
		// 		var md5_value2 = md5(data2.toString());
		// 		updateAttachment(title,rev_num,doc,md5_value,data,assetName,data2,md5_value2);	
				
		// 	}else{
		// 		console.log("Error Reading Assets");
		// 		console.log(err);
		// 	}
		// });
		data2=fs.readFileSync(file);
		var assetNameHolder = file.split("/");
		var assetName = cleanUpString(assetNameHolder[assetNameHolder.length-1],"_");
		var md5_value2 = md5(data2.toString());
		updateAttachment(title,rev_num,doc,md5_value,data,assetName,data2,md5_value2);
	})
}
//params:
//data - html data of our target document
//return:
// Array of assets with MD5
//
function getAllAssets(data,title)
{
	var Assets = new Array();
	//var imageRegEx = /<img.+src.+\/>/gmi;
	var imageCatch = new Array();
	
	//var cssRegEx =/<link.+rel=('|")stylesheet('|").+href.+css.+>/gmi;
	
	var cssCatch = new Array();
	collectRegEx(data, cssHrefRegEx, cssCatch);
	collectRegEx(data, imageSrcRegEx, imageCatch);
	//traverse each array and explode data to push to Assets
	//console.log("extracting href");
	stackAssets(cssCatch,Assets);
	//console.log("extracting images");
	stackAssets(imageCatch,Assets);
	console.log("Assets List for "+title);
	console.log(Assets);
	return Assets;
}
function cleanUpString(string,replaceString)
{
	var specialChars = /[&\/\\#,+()$~%.'":*?<>{} -]/g;
	string = string.replace(specialChars,replaceString);
	return string;
}
function collectRegEx(data, regExStatement, targetArray)
{
	var temp = '';
	do{
		temp = regExStatement.exec(data);
		if(temp){
			//console.log(temp[0]);
			targetArray.push(temp[0]);
		}
		//console.log("still processing css "+title);
	}while(temp)
}
//params:
// assetsArray - an array of possible assets
//returns:
// Assets - cleaned up list of real assets of the webpage
function stackAssets(assetArray,Assets)
{
	var temp;
	for(var x=0;x<assetArray.length;x++)
	{
		temp='';
		//check if our keyDir exists in the string
		if(assetArray[x].search(keyDir)!=-1)
		{
			//split by the key keyDir (which is _assets)
			temp = assetArray[x].split(keyDir);
			temp = temp[temp.length-1];
			//replace all special characters to ''
			temp = temp.replace(/'/g,'');
			//check if file exists
			if(fs.existsSync(assetsDir+temp))
			{
				Assets.push(assetsDir+temp);
			}
		}
	}
	//return Assets;
}