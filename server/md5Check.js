//require MD5 - using https://www.npmjs.org/package/md5-jkmyers
var fs = require('fs');
var md5 = require('md5-jkmyers');
var rootDIR = 'Library';
// Remember to check both English and French folders
// Maybe we should place these "folders" as part of a config file?
// for now let's hardcode this
var directories = [''];
var myfiles = [];

var path = './'+rootDIR+'/';
var keyDir = 'assets';
var assetsDir = './'+rootDIR+'/'+'assets';
var listImages = ["png","jpg","bmp","gif","jpeg","svg"];
var imageSrcRegEx = /(src).+('|").+\.(png|jpg|jpeg|bmp|gif|svg)('|")/gmi;
var cssHrefRegEx = /(href).+('|").+\.css('|")/gmi;
GLOBAL.filecounter = 0;

module.exports.readLibFiles = function(req,res){
	directories.forEach(directoryRead);
	if(res!=null)
	{
		res.send("Processing Files");
	}
}

function directoryRead(dir)
{
	fs.readdir(path+dir,function(err,files){
		if(err==null)
		{
			if(files instanceof Array){
				files.forEach(function(file){
					checkFile(file,dir)
				});
			}else{
				checkFile(files,dir);
			}
			processFiles();
		}else{
			console.log('Error in reading Directory:');
			console.log(err);
		}
	});
}

function checkFile(file,dir)
{
	var stats = fs.lstatSync(path+file);
	if(stats.isFile())
	{
		myfiles.push(path+file);
	}
}

function processFiles()
{
	AssetList = new Array();
	HashList = new Array();
	myfiles.forEach(function(filePath){
		GLOBAL.filecounter++;
		AssetList[GLOBAL.filecounter] = new Array();
		HashList[GLOBAL.filecounter] = new Array();
		getAssets(filePath, AssetList[GLOBAL.filecounter], HashList[GLOBAL.filecounter]);
		checkMD5(HashList[GLOBAL.filecounter],AssetList[GLOBAL.filecounter]);
	})
}

function getAssets(filePath, myAssets, myMainHash)
{
	// data=fs.readFileSync(target);
	// var temp_filename = file.split("/");
	// var temp_id = (temp_filename[temp_filename.length-1]).split(".");
	// var title = cleanUpString(temp_id[0],"_");
	// var md5_value = md5(data.toString());
	// console.log("id : "+title);
	// console.log("Generating MD5 -> "+md5_value);
	myMainHash['hash'] = '';
	myMainHash['bin'] = getMD5file(filePath,myMainHash,myAssets,true);
	myMainHash['filename'] = getMainFileName(filePath);
	// console.log('myMainHash');
	// console.log(myMainHash);
	// console.log('myAssets');
	// console.log(myAssets);
}

function getMainFileName(filePath)
{
	var assetNameHolder = filePath.split("/");
	var temp_id = (assetNameHolder[assetNameHolder.length-1]).split(".");
	var assetName = cleanUpString(temp_id[0],"_");
	return assetName;
}

function getFileName(filePath)
{
	var assetNameHolder = filePath.split("/");
	var assetName = cleanUpString(assetNameHolder[assetNameHolder.length-1],"_");
	return assetName;
}

function getMD5file(filePath,hash,myAssets,rewrite)
{
	var data = fs.readFileSync(filePath);
	var md5_value = md5(data.toString());
	var normal = true;
	var filename = getMainFileName(filePath);
	if(rewrite==true)
	{
		var temp = getAllAssets(data, filename);
		temp.forEach(function(data){
			myAssets.push(data);
		})
		var dataString = rewriteHTML(data);
		
	}else{
		var dataString = data.toString('utf8');
		if(listImages.indexOf(rewrite)!=-1)
		{
			normal=false;
			dataString = data;
		}
	}
	var binStore = new Buffer(dataString.length);
	// if(normal)
	// {
	// 	binStore.write(dataString,0,dataString.length,'base64');
	// }else{
		binStore.write(dataString);
	//}
	hash['hash']=md5_value;
	return binStore;
}

function checkMD5(myMainHash,myAssets)
{
	var myErr = false;
	GLOBAL.db.get(myMainHash['filename'],{conflicts:true},function(err,doc){
		if(err==null)
		{
			if(doc.file_hashes[myMainHash['filename']]!=undefined)
			{
				if(doc.file_hashes[myMainHash['filename']] != myMainHash['hash'])
				{
					updateDocument(myMainHash,myAssets,doc,false);
				}else{
					updateDocument(myMainHash,myAssets,doc,true);
				}
			}else{
				console.log('doc.file_hashes[myMainHash['+myMainHash['filename']+']] -> undefined');
				updateDocument(myMainHash,myAssets,doc,false);
			}
		}else{
			if(err.status == 404)
			{
				console.log('File not found - Updating -> '+myMainHash['filename']);
				updateDocument(myMainHash,myAssets,doc,false);
			}else{
				console.log('Error Reading DB');
				console.log(err);
				if(err.message!=undefined)
				{
					console.log(err);
					myErr = true;
				}else{
					setImmediate(checkMD5,myMainHash,myAssets);
				}
			}
		}
		if(myErr==true)
		{
			console.log( myMainHash['filename']+' does not need updating');
		}
	});
}

function updateDocument(myMainHash,myAssets,doc,attachmentFlag)
{
	console.log("Updating Document ->"+myMainHash['filename']);
	var AssetStore = {};
	var myDoc = {
		'file_hashes':{

		}
	};
	// console.log("myAssets");
	// console.log(myAssets);
	for(var x=0;x<myAssets.length;x++)
	{
		filePath = myAssets[x];
		console.log("getting asset : "+filePath);
		var filename = getFileName(filePath);
		var type = '';
		var data = [];
		var bin;
		type = identifyEXT(filename);
		bin = getMD5file(filePath,data,filePath,null,type);
		AssetStore[filename] = {
			"filename":filename,
			"hash":data['hash'],
			"bin":bin,
			"type":type
		};
		

		// myDoc['_attachments'][filename] = {
		// 	"content_type":type,
		// 	"data":AssetStore[filename]['bin']
		// };

		myDoc['file_hashes'][filename]=AssetStore[filename]['hash'];
	}

	myDoc['file_hashes'][myMainHash['filename']]=myMainHash['hash'];
	// myDoc['_attachments'][myMainHash['filename']]=myMainHash['bin'];
	// myDoc['_attachments'][myMainHash['filename']] = {
	// 		"content_type":"text\/plain",
	// 		"data":myMainHash['bin']
	// 	};
	// console.log("myDoc");
	// console.log(myDoc);
	// console.log("doc");
	// console.log(doc);
	console.log("AssetStore");
	console.log(AssetStore);
	if(attachmentFlag==false)
	{
		console.log("Saving "+myMainHash['filename']+" hash");
		putRecord(myMainHash,myAssets,AssetStore,myDoc,doc);
	}else{
		console.log("Checking attachments hash");
		processAttachments(myMainHash,myAssets,AssetStore,myDoc);
	}
}

function ReadRecord(myMainHash,myAssets,AssetStore,myDoc,attachment,filename,mainHashflag)
{
	// console.log("Reading -> "+myMainHash['filename']);
	// console.log("filename : "+filename);
	GLOBAL.db.get(myMainHash['filename'], {conflicts:true}, function(err,doc2){
		if(err==null)
		{
			if(attachment==false)
			{
				putRecord(myMainHash,myAssets,AssetStore,myDoc,doc2);
			}else{
				saveAttachment(myMainHash,myAssets,AssetStore,myDoc,attachment,filename,mainHashflag,doc2);
			}
		}else{
			if(err.status==409)
			{
				setImmediate(ReadRecord,myMainHash,myAssets,AssetStore,myDoc,attachment,filename,mainHashflag);
			}else if(err.message==undefined){
				setImmediate(ReadRecord,myMainHash,myAssets,AssetStore,myDoc,attachment,filename,mainHashflag);
			}else{
				console.log("Error Reading Record");
				console.log(err);
			}
		}
	});
}

function putRecord(myMainHash,myAssets,AssetStore,myDoc,doc){
	if(doc!=undefined)
	{
		myMainHash['needUpdate']=false;
		console.log("Updating -> "+myMainHash['filename']);
		var tempDoc = doc;
		tempDoc.file_hashes[myMainHash['filename']]=myMainHash['hash'];
		myDoc = tempDoc;
		GLOBAL.db.put(myDoc,myMainHash['filename'],doc._rev,function(err,doc2){
			if(err==null)
			{
				console.log("saved document -> "+myMainHash['filename']);
				processAttachments(myMainHash,myAssets,AssetStore,myDoc);
			}else{
				if(err.status==409 || err.status == 404)
				{
					setImmediate(ReadRecord,myMainHash,myAssets,AssetStore,myDoc,false,'','');
				}else{
					console.log("Error Updating Record");
					console.log(err);
				}
			}
		});
	}else{
		myMainHash['needUpdate']=true;
		//reset to save only the main file's hash
		// myDoc = {
		// 	'file_hashes':{

		// 	}
		// };
		myDoc['file_hashes'][myMainHash['filename']]=myMainHash['hash'];
		console.log("Writing -> "+myMainHash['filename']);
		GLOBAL.db.put(myDoc,myMainHash['filename'],function(err,doc2){
			if(err==null)
			{
				console.log("saved document -> "+myMainHash['filename']);
				processAttachments(myMainHash,myAssets,AssetStore,myDoc);
			}else{
				if(err.status==409 || err.status == 404)
				{
					setImmediate(ReadRecord,myMainHash,myAssets,AssetStore,myDoc,false,'','');
				}else{
					console.log("Error Writing Record");
					console.log(err);
				}
			}
		});
	}
}

function processAttachments(myMainHash,myAssets,AssetStore,myDoc)
{
	// console.log("myAssets");
	// console.log(myAssets);
	// console.log(AssetStore);
	for(var x=0;x<myAssets.length;x++)
	{
		var filePath = myAssets[x];
		var filename = getFileName(filePath);
		console.log("Processing asset : "+filename);
		// console.log(AssetStore[filename]);
		// for(var y =0;y<3;y++)
		// {
			ReadRecord(myMainHash,myAssets,AssetStore,myDoc,true,filename,false);
		// }
	}	
	console.log("Processing main file : "+myMainHash['filename']);
	ReadRecord(myMainHash,myAssets,AssetStore,myDoc,true,myMainHash['filename'],true);
	// ReadRecord(myMainHash,myAssets,AssetStore,myDoc,true,myMainHash['filename'],true);
}

function saveAttachment(myMainHash,myAssets,AssetStore,myDoc,attachment,filename,mainHashflag,doc2)
{
	console.log("Save Attachment myAssets");
	console.log(myAssets);
	console.log("Save Attachment AssetStore");
	console.log(AssetStore);
	var need_update=false;
	if(mainHashflag==true)
	{
		console.log("Processing Main File : "+filename);
		//need_update=true;
	}else{
		console.log("Processing asset : "+filename);
	}
	try{
		if(doc2.file_hashes[filename]!=undefined && doc2._attachments[filename]!=undefined)
		{
			if(doc2.file_hashes[filename]!=AssetStore[filename]['hash'])
			{
				need_update=true;
			}
		}else{
			need_update=true;
		}
		if(myMainHash['needUpdate']==true)
		{
			need_update=true;	
		}
	}catch(error){
		need_update=true;
	}
	if(need_update==false)
	{
		console.log("No Need to update the record -> "+myMainHash['filename']);
	}else{
		console.log("Updating record -> "+myMainHash['filename']+" for "+filename);
		var attachment;
		var type;
		if(mainHashflag==true)
		{
			attachment = myMainHash['bin'];
			type = "text\/plain";
		}else{
			attachment = AssetStore[filename]['bin'];
			type = AssetStore[filename]['type'];
		}
		console.log("Type: "+type);
		console.log("sending command to update ... ");
		GLOBAL.db.putAttachment(myMainHash['filename'], filename, doc2._rev, attachment, type, function(err,doc){
			if(err==null)
			{
				console.log("Saved Attachment -> "+filename+" for "+myMainHash['filename']);
				if(mainHashflag!=true)
				{
					saveHash(myMainHash['filename'],filename,AssetStore[filename]['hash'],myMainHash,myAssets,AssetStore,myDoc,attachment,filename,mainHashflag);
				}else{
					finalCheck(myMainHash,myAssets,AssetStore,myDoc,attachment,filename,mainHashflag);
				}
			}else{
				if(err.status==409)
				{
					console.log("409 in update");
					console.log("resending Read Record command...");
					console.log("for "+filename);
					if(mainHashflag!=true)
					{
						ReadRecord(myMainHash,myAssets,AssetStore,myDoc,true,filename,false);
					}else{
						ReadRecord(myMainHash,myAssets,AssetStore,myDoc,true,myMainHash['filename'],true);
					}
				}else{
					console.log("Error Saving Document");
					console.log(err);
				}
			}
		});
	}
}

function saveHash(id,filename2,hashvalue,myMainHash,myAssets,AssetStore,myDoc,attachment,filename,mainHashflag)
{
	console.log("id : "+id+" filename : "+filename2+" hashvalue : "+hashvalue);
	GLOBAL.db.get(id, {conflicts:true}, function(err,doc2){
		if(err==null)
		{
			var tempDoc = doc2;
			tempDoc.file_hashes[filename2]=hashvalue;
			myDoc = tempDoc;
			GLOBAL.db.put(myDoc,id,doc2._rev,function(err,doc3){
				if(err==null)
				{
					console.log("Saving "+filename+" hash successful.");
					finalCheck(myMainHash,myAssets,AssetStore,myDoc,attachment,filename,mainHashflag);
				}else{
					if(err.status==409)
					{
						console.log("409 Error saving Hash");
						console.log("Resaving Hash");
						setImmediate(saveHash,id,filename,hashvalue,myMainHash,myAssets,AssetStore,myDoc,attachment,filename,mainHashflag);
					}else{
						console.log("Error Saving Hash for -> "+filename+" from "+id);
						console.log(err);
						finalCheck(myMainHash,myAssets,AssetStore,myDoc,attachment,filename,mainHashflag);
					}
				}
			});
		}else{
			console.log("Error Reading DB Record -> "+id);
			console.log("Reading again ...");
			setImmediate(saveHash,id,filename,hashvalue,myMainHash,myAssets,AssetStore,myDoc,attachment,filename,mainHashflag);
		}
	});
}

function finalCheck(myMainHash,myAssets,AssetStore,myDoc,attachment,filename,mainHashflag)
{
	GLOBAL.db.get(myMainHash['filename'],{conflicts:true},function(err,doc){
		if(err==null)
		{
			var breakFlag = false;
			//check if all items in AssetStore[filename] exist
			for(var x=0; x < myAssets.length; x++)
			{
				var temp = myAssets[x];
				var key = getFileName(temp);
				try{
					if(typeof(doc['_attachments'][key]) == 'undefined')
					{
						ReadRecord(myMainHash,myAssets,AssetStore,myDoc,true,key,false);
						breakFlag = true;
					}
				}catch(error){
					ReadRecord(myMainHash,myAssets,AssetStore,myDoc,true,key,false);
					breakFlag = true;
				}
				try{
					if(typeof(doc.file_hashes[key]) == 'undefined' && breakFlag == false)
					{
						saveHash(myMainHash['filename'],key,AssetStore[key]['hash'],myMainHash,myAssets,AssetStore,myDoc,attachment,filename,mainHashflag);
						breakFlag = true;
					}
				}catch(error){
					saveHash(myMainHash['filename'],key,AssetStore[key]['hash'],myMainHash,myAssets,AssetStore,myDoc,attachment,filename,mainHashflag);
					breakFlag = true;
				}
				try{
					var temp2 = AssetStore[key]['bin'];
					if(doc._attachment[key]['length'] != temp2.length && breakFlag == false)
					{
						ReadRecord(myMainHash,myAssets,AssetStore,myDoc,true,key,false);
						breakFlag = true;	
					}
				}catch(error){
					//do nothing
				}
				
				if(breakFlag==true)
				{
					break;
				}
			}
			if(breakFlag==false)
			{
				try{
					if(typeof(doc['_attachments'][myMainHash['filename']])=='undefined')
					{
						ReadRecord(myMainHash,myAssets,AssetStore,myDoc,true,myMainHash['filename'],true);
						breakFlag=true;
					}
				}catch(error){
					ReadRecord(myMainHash,myAssets,AssetStore,myDoc,true,myMainHash['filename'],true);
					breakFlag=true;
				}
				try{
					var temp3 = myMainHash['bin'];
					if(doc._attachment[myMainHash['filename']]['length'] != temp3.length && breakFlag == false)
					{
						ReadRecord(myMainHash,myAssets,AssetStore,myDoc,true,myMainHash['filename'],true);
						breakFlag=true;
					}
				}catch(error){

				}
			}
		}else{
			setImmediate(finalCheck,myMainHash,myAssets,AssetStore,myDoc,attachment,filename,mainHashflag);
		}
	});
}
//*******************************various util functions***********************************************
function cleanUpString(string,replaceString)
{
	var specialChars = /[&\/\\#,+()$~%.'":*?<>{} -]/g;
	string = string.replace(specialChars,replaceString);
	return string;
}
function getAllAssets(data, filename)
{
	var Assets = new Array();
	//var imageRegEx = /<img.+src.+\/>/gmi;
	var imageCatch = new Array();
	
	//var cssRegEx =/<link.+rel=('|")stylesheet('|").+href.+css.+>/gmi;
	
	var cssCatch = new Array();
	collectRegEx(data, cssHrefRegEx, cssCatch);
	collectRegEx(data, imageSrcRegEx, imageCatch);
	//traverse each array and explode data to push to Assets
	//console.log("extracting href");
	stackAssets(cssCatch,Assets);
	//console.log("extracting images");
	stackAssets(imageCatch,Assets);
	console.log("Assets List for "+filename);
	console.log(Assets);
	return Assets;
}
function stackAssets(assetArray,Assets)
{
	var temp;
	for(var x=0;x<assetArray.length;x++)
	{
		temp='';
		//check if our keyDir exists in the string
		if(assetArray[x].search(keyDir)!=-1)
		{
			//split by the key keyDir (which is _assets)
			temp = assetArray[x].split(keyDir);
			temp = temp[temp.length-1];
			//replace all special characters to ''
			temp = temp.replace(/'/g,'');
			//check if file exists
			if(fs.existsSync(assetsDir+temp))
			{
				Assets.push(assetsDir+temp);
			}
		}
	}
	//return Assets;
}
function collectRegEx(data, regExStatement, targetArray)
{
	var temp = '';
	do{
		temp = regExStatement.exec(data);
		if(temp){
			//console.log(temp[0]);
			targetArray.push(temp[0]);
		}
		//console.log("still processing css "+title);
	}while(temp)
}
function rewriteHTML(data,dataString)
{
	var cssCatch = new Array();
	var imageCatch = new Array();
	var temp;
	var mybody = data.toString('utf8');
	collectRegEx(data, cssHrefRegEx, cssCatch);
	collectRegEx(data, imageSrcRegEx, imageCatch);
	cssCatch.forEach(function(cssValue){
		temp = cssValue.split("/");
		var filename = temp[temp.length-1];
		filename.replace(/'/g,'');
		//console.log(filename);
		//console.log("replacing "+cssValue+" to "+"href='"+filename);
		var myregex = new RegExp(cssValue,"gmi");
		mybody = mybody.replace(myregex,"href='"+filename);
	});
	imageCatch.forEach(function(imgValue){
		temp = imgValue.split("/");
		var filename = temp[temp.length-1];
		filename.replace(/'/g,'');
		// console.log(filename);
		// console.log("replacing "+imgValue+" to "+"href='"+filename);
		var myregex = new RegExp(imgValue,"gmi");
		mybody = mybody.replace(myregex,"href='"+filename);
	});
	return mybody;
}
function identifyEXT(assetName)
{
	var extensionHolder = assetName.split("_");
	var extension = extensionHolder[extensionHolder.length-1];
	var type = "text\/plain";
	var typeIndex = listImages.indexOf(extension);
	if(typeIndex!=-1)
	{
		type="image\/"+extension;
		if(extension=="jpg")
		{
			type="image\/jpeg";
		}
		if(extension=="svg")
		{
			type="image\/svg+xml";
		}
	}
	return type;
}